core = 8.x
api = 2

; core
projects[drupal][type] = "core"
projects[drupal][download][type] = "get"
projects[drupal][download][url] = "https://ftp.drupal.org/files/projects/drupal-8.9.14.tar.gz"


; Modules

projects[admin_toolbar][version] = "2.4"

projects[advagg][version] = "4.1"

projects[backup_migrate][version] = "4.1"

projects[better_exposed_filters][version] = "3.0-alpha6"

projects[blazy][version] = "1.0-rc6"

projects[block_visibility_groups][version] = "1.3"

projects[captcha][version] = "1.0-beta4"

projects[colorbox][version] = "1.6"

projects[contribute][version] = "1.0-beta8"

projects[ctools][version] = "3.4"

projects[devel][version] = "2.1"

projects[draggableviews][version] = "1.2"

projects[editor_file][version] = "1.5"

projects[editor_advanced_link][version] = "1.8"

projects[entity_reference_revisions][version] = "1.9"

; projects[exif][version] = "1.2"
; geeft errors

projects[fontyourface][version] = "3.3"

projects[metatag][version] = "1.15"

projects[matomo][version] = "1.11"

projects[paragraphs][version] = "1.12"

projects[pathauto][version] = "1.8"

projects[recaptcha][version] = "2.5"

projects[robotstxt][version] = "1.4"

projects[simple_sitemap][version] = "3.9"

projects[slick][version] = "1.2"

projects[slick_extras][version] = "1.0-rc3"

projects[slick_views][version] = "1.0"

projects[term_condition][version] = "1.2"
; projects[term_condition][patch][] = "https://www.drupal.org/files/issues/term_condition-2747575-9.patch"

projects[token][version] = "1.9"

projects[translatable_menu_link_uri][version] = "1.2"

projects[webform][version] = "5.25"


; Themes

projects[zurb_foundation][type] = theme
projects[zurb_foundation][download][type] = git
projects[zurb_foundation][download][url] = https://webfed@bitbucket.org/webfed/zurb_foundation.git


; Libaries

libraries[blazy][download][type] = "git"
libraries[blazy][download][url] = "https://github.com/dinbror/blazy.git"
libraries[blazy][directory_name] = "blazy"
libraries[blazy][type] = "library"

libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox.git"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"


libraries[slick][download][type] = "git"
libraries[slick][download][url] = "https://github.com/kenwheeler/slick.git"
libraries[slick][directory_name] = "slick"
libraries[slick][type] = "library"