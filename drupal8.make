core = 8.x
api = 2

; core
projects[drupal][type] = "core"
projects[drupal][download][type] = "get"
projects[drupal][download][url] = "https://ftp.drupal.org/files/projects/drupal-8.9.19.tar.gz"


; Modules

projects[admin_toolbar][version] = "2.5"

projects[auto_entitylabel][version] = "3.0-beta4"

projects[advagg][version] = "4.1"

projects[backup_migrate][version] = "4.2"

; projects[better_exposed_filters][version] = "5.0-beta1"

projects[blazy][version] = "1.0-rc6"

projects[block_permissions][version] = "1.2"

projects[block_class][version] = "1.3"

projects[block_visibility_groups][version] = "1.4"

projects[captcha][version] = "1.2"

projects[classy_paragraphs][version] = "1.0-beta3"

projects[colorbox][version] = "1.7"

projects[contribute][version] = "1.0-beta8"

projects[ctools][version] = "3.7"

projects[crop][version] = "1.5"

projects[draggableviews][version] = "1.2"

projects[dropdown_language][version] = "2.7"

projects[editor_file][version] = "1.5"

projects[editor_advanced_link][version] = "1.9"

projects[entity_reference_revisions][version] = "1.9"

projects[exif_orientation][version] = "1.1"

projects[external_links_new_tab][version] = "1.2"

projects[fontawesome][version] = "2.19"

// not needed projects[features][version] = "3.12"

// not needed projects[fontyourface][version] = "3.6"

projects[image_widget_crop][version] = "2.3"

projects[layout_bg][version] = "1.1"

projects[metatag][version] = "1.16"

projects[matomo][version] = "1.11"

projects[mailchimp][version] = "1.12"

projects[migrate_upgrade][version] = "3.2"

// not needed projects[office_hours][version] = "1.3"

projects[paragraphs][version] = "1.12"

projects[pathauto][version] = "1.8"

projects[permissions_by_term][version] = "2.34"

projects[private_files_download_permission][version] = "2.2"

projects[recaptcha][version] = "2.5"

projects[redirect][version] = "1.6"

projects[readonlymode][version] = "1.1"

projects[responsive_favicons][version] = "1.6"

projects[robotstxt][version] = "1.4"

projects[simple_sitemap][version] = "3.10"

projects[slick][version] = "1.2"

projects[slick_extras][version] = "1.0-rc4"

projects[slick_views][version] = "1.0"

projects[smtp][version] = "1.0"

projects[svg_formatter][version] = "1.14"

projects[term_condition][version] = "1.2"
; projects[term_condition][patch][] = "https://www.drupal.org/files/issues/term_condition-2747575-9.patch"

projects[token][version] = "1.9"

projects[translatable_menu_link_uri][version] = "1.2"

projects[user_default_page][version] = "2.0-rc1"

projects[views_bulk_operations][version] = "3.13"

projects[webform][version] = "5.27"


; Modules only for development

projects[config_update][version] = "1.7"

projects[devel][version] = "2.1"

projects[features][version] = "3.8"

projects[migrate_upgrade][version] = "3.2"

projects[migrate_plus][version] = "4.2"


; Themes

projects[zurb_foundation][version] = "6.0-alpha2"



; Libraries

libraries[blazy][download][type] = "git"
libraries[blazy][download][url] = "https://github.com/dinbror/blazy.git"
libraries[blazy][directory_name] = "blazy"
libraries[blazy][type] = "library"

libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox.git"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"

libraries[cropper][download][type] = "git"
libraries[cropper][download][url] = "https://github.com/fengyuanchen/cropper.git"
libraries[cropper][directory_name] = "cropper"
libraries[cropper][type] = "library"

libraries[inputmask][download][type] = "git"
libraries[inputmask][download][url] = "https://github.com/RobinHerbots/Inputmask.git"
libraries[inputmask][directory_name] = "inputmask"
libraries[inputmask][type] = "library"


libraries[slick][download][type] = "git"
libraries[slick][download][url] = "https://github.com/kenwheeler/slick.git"
libraries[slick][directory_name] = "slick"
libraries[slick][type] = "library"


