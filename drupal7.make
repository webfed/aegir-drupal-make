core = 7.x
api = 2

; core
projects[drupal][type] = "core"
projects[drupal][download][type] = "get"
projects[drupal][download][url] = "https://ftp.drupal.org/files/projects/drupal-7.86.tar.gz"


; Modules
projects[addressfield][version] = "1.3"

projects[admin][version] = "2.0-beta3"

projects[admin_menu][version] = "3.0-rc6"

projects[admin_views][version] = "1.8"

projects[advagg][version] = "2.35"

projects[advanced_help][version] = "1.6"

projects[antibot][version] = "1.2"

projects[audiofield][version] = "1.5"

projects[availability_calendars][version] = "5.8"

projects[backup_migrate][version] = "3.10"

projects[backup_migrate_files][version] = "1.x-dev"

projects[beautytips][version] = "2.2"

projects[better_exposed_filters][version] = "3.6"

projects[better_formats][version] = "1.0-beta2"

projects[block_class][version] = "2.4"

projects[boost][version] = "1.2"


projects[bxslider][version] = "1.4"

; projects[bxslider_views_slideshow][version] = "1.51"
; Gepatchte versie anders errors met PHP 7.1
projects[bxslider_views_slideshow][version] = "1.51"
projects[bxslider_views_slideshow][type] = module
projects[bxslider_views_slideshow][download][type] = git
projects[bxslider_views_slideshow][download][url] = https://bitbucket.org/webfed/bxslider_views_slideshow.git

projects[cacheexclude][version] = "2.3"

projects[calendar][version] = "3.6"

projects[cck][version] = "3.0-alpha3"

projects[ccl][version] = "1.7"

projects[ckeditor][version] = "1.22"

projects[ckeditor_link][version] = "2.4"

projects[ckeditor_link_file][version] = "1.3"

projects[classy_paragraphs][version] = "1.0"

projects[clock][version] = "1.2"

projects[collapsiblock][version] = "2.0"

projects[colorbox][version] = "2.15"

projects[composer_manager][version] = "1.8"

projects[context][version] = "3.10"

projects[cs_adaptive_image][version] = "1.1"

projects[css3pie][version] = "2.1"

projects[css_emimage][version] = "1.3"

projects[crumbs][version] = "2.7"

projects[ctools][version] = "1.20"

projects[cufon][version] = "2.1"

projects[custom_search][version] = "1.20"

projects[diff][version] = "3.4"

projects[date][version] = "2.12"

projects[delta][version] = "3.0-beta11"

projects[devel][version] = "1.7"


;; Versie 2.1 geeft errors nu de most recent dev version was tot 20200409 7.x-2.1+9-dev
projects[draggableviews][version] = "2.x-dev"
;projects[draggableviews][type] = module
;projects[draggableviews][download][type] = git
;projects[draggableviews][download][url] = https://webfed@bitbucket.org/webfed/draggableviews.git

projects[ds][version] = "2.16"

projects[ed_classified][version] = "3.1"

projects[elements][version] = "1.5"

projects[email][version] = "1.3"

projects[entity][version] = "1.10"

projects[entitycache][version] = "1.5"

projects[entityreference][version] = "1.5"

projects[exclude_node_title][version] = "1.9"

projects[expire][version] = "2.0-rc4"

projects[extlink][version] = "1.21"

projects[facetapi][version] = "1.9"

projects[facetapi_slider][version] = "1.0"

projects[favorites][type] = module
projects[favorites][download][type] = git
projects[favorites][download][url] = https://webfed@bitbucket.org/webfed/favorites.git

projects[fb_autopost][version] = "1.4"

projects[fb_social][version] = "2.0-beta4"

projects[features][version] = "2.13"

projects[field_collection][version] = "1.2"

projects[field_collection_table][version] = "1.0-beta5"

projects[field_formatter_css_class][version] = "1.3"

projects[field_formatter_settings][version] = "1.1"

projects[field_group][version] = "1.6"

projects[filefield_sources][version] = "1.11"

projects[filefield_sources_plupload][version] = "1.1"

projects[file_entity][version] = "2.35"

projects[fitvids][version] = "1.17"

projects[flexslider][version] = "2.0-rc2"

projects[fontawesome][version] = "3.13"

projects[fontyourface][version] = "2.8"

projects[forward][version] = "2.1"

projects[fusion_accelerator][version] = "2.0-beta1"

projects[galleryformatter][version] = "1.5"

projects[globalredirect][version] = "1.6"

projects[google_analytics][version] = "2.6"

projects[google_recaptcha][version] = "1.2"

projects[guestbook][version] = "2.x-dev"

projects[fences][version] = "1.2"

projects[honeypot][version] = "1.26"

projects[html5_tools][version] = "1.3"

projects[httprl][version] = "1.14"

projects[i18n][version] = "1.31"

projects[i18n_contrib][version] = "1.0-alpha2"

projects[imagecache_actions][version] = "1.13"

projects[image_field_caption][type] = module
projects[image_field_caption][download][type] = git
projects[image_field_caption][download][url] = https://webfed@bitbucket.org/webfed/image_field_caption.git

projects[imagemagick][version] = "1.0"

projects[imce][version] = "1.11"

projects[imce_wysiwyg][version] = "1.0"

projects[insert_view][version] = "2.0"

projects[jplayer][version] = "2.0"

projects[jquery_update][version] = "2.7"

projects[label_help][version] = "1.2"

projects[languageicons][version] = "1.1"

projects[libraries][version] = "2.5"

projects[link][version] = "1.9"

projects[link_css][version] = "1.1"

projects[linkit][version] = "2.7"

projects[logintoboggan][version] = "1.5"

projects[mail_edit][version] = "1.2"

projects[mailchimp][version] = "5.6"

projects[mailsystem][version] = "2.35"

projects[masonry][version] = "3.0-beta1"

projects[masonry_fields][version] = "3.0-beta1"
projects[masonry_fields][patch] = "https://www.drupal.org/files/2021825-undefined-index-5.patch"

projects[masonry_views][version] = "3.0"

projects[masquerade][version] = "1.0-rc7"

projects[matomo][version] = "2.12"

projects[media][version] = "2.27"

projects[media_youtube][version] = "3.10"

projects[media_vimeo][version] = "2.1"

projects[menu_admin_per_menu][version] = "1.1"

projects[menu_firstchild][version] = "1.1"

projects[menu_token][version] = "1.0-beta7"

projects[menuux][version] = "1.0-beta4"

projects[metatag][version] = "1.28"

projects[migrate][version] = "2.11"

projects[migrate_extras][version] = "2.5"

projects[mimemail][version] = "1.2"

projects[module_filter][version] = "2.2"

projects[mobile_navigation][type] = module
projects[mobile_navigation][download][type] = git
projects[mobile_navigation][download][url] = https://webfed@bitbucket.org/webfed/mobile_navigation.git

projects[nice_menus][version] = "2.5"

projects[node_export][version] = "3.1"

projects[oauth][version] = "3.4"

projects[omega_tools][version] = "3.0-rc4"

projects[openlayers][version] = "2.0-beta11"

projects[page_title][version] = "2.7"

projects[panels][version] = "3.10"

projects[paragraphs][version] = "1.0-rc5"

projects[pathauto][version] = "1.3"

projects[pathologic][version] = "2.12"

projects[persistent_login][version] = "1.1"

projects[picture][version] = "2.13"

projects[plup][version] = "1.0-alpha1"

projects[plupload][version] = "1.7"

projects[prev_next][version] = "2.x-dev"

projects[privatemsg][version] = "1.4"

projects[profile2][version] = "1.7"

projects[proj4js][version] = "1.2"

projects[queue_mail][version] = "1.6"

projects[quicktabs][version] = "3.8"

projects[readonlymode][version] = "1.2"

projects[redirect][version] = "1.0-rc4"

projects[resp_img][type] = module
projects[resp_img][download][type] = git
projects[resp_img][download][url] = https://webfed@bitbucket.org/webfed/resp_img.git
projects[resp_img][download][branch] = master

projects[responsive_menus][version] = "1.7"

projects[robotstxt][version] = "1.4"

projects[rules][version] = "2.13"

projects[scheduler][version] = "1.6"

projects[schemaorg][version] = "1.0-rc1"

projects[search404][version] = "1.6"

projects[search_api][version] = "1.28"

projects[search_api_db][version] = "1.8"

projects[search_api_ranges][version] = "1.5"

projects[select_or_other][version] = "2.24"

projects[seo_checker][version] = "1.8"

// NOT SUPPORTED projects[seo_checklist][version] = "4.1"

// projects[service_links][version] = "2.4"

projects[service_links][type] = module
projects[service_links][download][type] = git
projects[service_links][download][url] = https://webfed@bitbucket.org/webfed/service_links.git
projects[service_links][download][branch] = master


projects[sharebar][version] = "1.1"

projects[shs][version] = "1.8"

; needed for Theme developer
projects[simplehtmldom][version] = "2.1"

projects[skinr][version] = "2.0"

projects[simple_mobile_redirect][version] = "1.1"

projects[simplenews][version] = "1.1"

projects[site_map][version] = "1.3"

projects[site_verify][version] = "1.2"

projects[smtp][version] = "1.7"

projects[socialmedia][version] = "1.0-beta16"

projects[superfish][version] = "2.0"

projects[taxonomy_access][version] = "1.0"

projects[taxonomy_menu][version] = "1.6"

projects[telephone][version] ="1.0-alpha1"

projects[token][version] = "1.9"

projects[transliteration][version] = "3.2"

projects[twitter][version] = "6.2"

projects[uuid][version] = "1.3"

projects[variable][version] = "2.5"

projects[videojs][version] = "3.0-alpha3"

projects[views][version] = "3.25"

projects[views_accordion][version] = "1.6"

projects[views_bulk_operations][version] ="3.6"

projects[views_cache_bully][version] ="3.1"

projects[views_content_cache][version] ="3.0-alpha3"

projects[views_export_xls][version] = "1.0"

;Disabled cause of dead links in makefile nu weer enabled voor 3.9
projects[views_slideshow][version] = "3.10"

;projects[views_slideshow][type] = module
;projects[views_slideshow][download][type] = git
;projects[views_slideshow][download][url] = https://webfed@bitbucket.org/webfed/views_slideshow.git
;projects[views_slideshow][download][branch] = master

projects[webform][version] = "4.24"

projects[webform2pdf][version] = "4.1"

projects[webform_clear][version] = "2.0"

projects[webform_confirm_email][version] = "2.18"

projects[webform_rules][version] = "1.6"

projects[webform_steps][version} = "1.0-alpha7"

projects[webform_validation][version] = "1.18"

projects[webform_mysql_views][version] = "1.1"

projects[widgets][version] = "1.0-rc1"

projects[wysiwyg][version] = "2.9"

projects[xmlsitemap][version] = "2.6"

projects[zurb_interchange][version] = "5.0"


; Features =====================================================================

projects[agenda_view][type] = module
projects[agenda_view][download][type] = git
projects[agenda_view][download][url] = https://webfed@bitbucket.org/webfed/agenda_view.git
projects[agenda_view][download][branch] = master
projects[agenda_view][subdir] = custom/features

projects[contactformulier][type] = module
projects[contactformulier][download][type] = git
projects[contactformulier][download][url] = https://webfed@bitbucket.org/webfed/contactformulier.git
projects[contactformulier][download][branch] = master
projects[contactformulier][subdir] = custom/features

projects[node_event][type] = module
projects[node_event][download][type] = git
projects[node_event][download][url] = https://webfed@bitbucket.org/webfed/node_event.git
projects[node_event][download][branch] = master
projects[node_event][subdir] = custom/features

projects[Responsive-imagestyles][type] = module
projects[Responsive-imagestyles][download][type] = git
projects[Responsive-imagestyles][download][url] = https://webfed@bitbucket.org/webfed/responsive-imagestyles.git
projects[Responsive-imagestyles][download][branch] = master
projects[Responsive-imagestyles][subdir] = custom/features

;projects[breakpoints_responsive_images][type] = module
;projects[breakpoints_responsive_images][download][type] = git
;projects[breakpoints_responsive_images][download][url] = https://webfed@bitbucket.org/webfed/breakpoints_responsive_images.git
;projects[breakpoints_responsive_images][download][branch] = master
;projects[breakpoints_responsive_images][subdir] = custom/features


; Patches
;projects[ga_tokenizer][patch][] = "https://drupal.org/files/issues/split-deprecated.1198504.patch"
;projects[rules][patch][] = "http://drupal.org/files/rules.state_.inc-ajax-error.patch"
;Todo
;Error: Unable to patch smtp with smtp-888856-2_0.patch.
;projects[smtp][patch][] = "http://drupal.org/files/issues/smtp-888856-2_0.patch"


; Themes

projects[acquia_marina][version] = "2.0-beta1"

projects[adaptivetheme][version] = "3.5"

projects[adminimal_theme][version] = "1.26"

projects[corolla][version] = "3.1"

projects[footheme][version] = "3.0-rc1"

projects[fusion][version] = "2.2"

projects[omega][version] = "3.1"

projects[sasson][version] = "2.10"

projects[zen][type] = theme
projects[zen][version] = "5.4"
projects[zen][download][type] = git
projects[zen][download][url] = http://git.drupal.org/project/zen.git

projects[zurb-foundation][version] = "4.0"
;Todo: patch fails
;projects[zurb-foundation][patch][] = "https://drupal.org/files/issues/zurb-foundation-array-2-string-2205041-6.patch"

; Libraries



libraries[bxslider][download][type] = "git"
libraries[bxslider][download][url] = "https://webfed@bitbucket.org/webfed/bxslider.git"
libraries[bxslider][directory_name] = "bxslider"
libraries[bxslider][type] = "library"

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox.git"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"

libraries[cufon][download][type] = "get"
libraries[cufon][download][url] = "http://cdnjs.cloudflare.com/ajax/libs/cufon/1.09i/cufon-yui.js"
libraries[cufon][directory_name] = "cufon"
libraries[cufon][type] = "library"



libraries[fitvids][download][type] = "git"s
libraries[fitvids][download][url] = "https://github.com:facebook/facebook-php-sdk-v4.git"
libraries[fitvids][directory_name] = "facebook-php-sdk"
libraries[fitvids][type] = "library"




libraries[fitvids][download][type] = "git"
libraries[fitvids][download][url] = "https://github.com/davatron5000/FitVids.js.git"
libraries[fitvids][directory_name] = "fitvids"
libraries[fitvids][type] = "library"

libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/archive/master.zip"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"


libraries[fontawesome][download][type] = "git"
libraries[fontawesome][download][url] = "https://github.com/FortAwesome/Font-Awesome.git"
libraries[fontawesome][directory_name] = "fontawesome"
libraries[fontawesome][type] = "library"


libraries[geoPHP][download][type] = "git"
libraries[geoPHP][download][url] = "https://github.com/phayes/geoPHP.git"
libraries[geoPHP][download][branch] = "master"
libraries[geoPHP][directory_name] = "geoPHP"
libraries[geoPHP][type] = "library"

libraries[jplayer][download][type] = "git"
libraries[jplayer][download][url] = "https://github.com/happyworm/jPlayer.git"
libraries[jplayer][directory_name] = "jplayer"
libraries[jplayer][type] = "library"

; GAAT DIT GOED? nee op 20181228 dus gewijzigd in raw
libraries[jquery.cycle][download][type] = "get"
libraries[jquery.cycle][download][url] = "https://bitbucket.org/webfed/jquery.cycle/raw/735112688813f112985c5e6773726d7f298b1022/jquery.cycle.all.js"
libraries[jquery.cycle][directory_name] = "jquery.cycle"
libraries[jquery.cycle][type] = "library"


libraries[json2][download][type] = "get"
libraries[json2][download][url] = "https://bitbucket.org/webfed/json-js/raw/3d7767b6b1f3da363c625ff54e63bbf20e9e83ac/json2.js"
libraries[json2][directory_name] = "json2"
libraries[json2][type] = "library"

libraries[mailchimp][download][type] = "get"
libraries[mailchimp][download][url] = "https://github.com/thinkshout/mailchimp-api-php/files/1361112/v1.0.8-package.zip"
libraries[mailchimp][directory_name] = "mailchimp"
libraries[mailchimp][type] = "library"

libraries[PIE][download][type] = "git"
libraries[PIE][download][url] = "https://github.com/lojjic/PIE.git"
libraries[PIE][directory_name] = "PIE"
libraries[PIE][type] = "library"

libraries[plupload][download][type] = "get"
libraries[plupload][download][url] = "https://github.com/moxiecode/plupload/archive/v1.5.8.zip"
libraries[plupload][patch][1903850] = "http://drupal.org/files/issues/plupload-1_5_8-rm_examples-1903850-16.patch"
libraries[plupload][directory_name] = "plupload"
libraries[plupload][type] = "library"

libraries[superfish][download][type] = "git"
libraries[superfish][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal.git"
libraries[superfish][directory_name] = "superfish"
libraries[superfish][type] = "library"

libraries[timepicker][download][type] = "git"
libraries[timepicker][download][url] = "https://github.com/wvega/timepicker.git"
libraries[timepicker][directory_name] = "wvega-timepicker"
libraries[timepicker][type] = "library"