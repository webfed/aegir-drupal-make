core = 8.x
api = 2

; core
projects[drupal][type] = "core"
projects[drupal][download][type] = "get"
projects[drupal][download][url] = "http://ftp.drupal.org/files/projects/drupal-8.3.4.tar.gz"


; Modules


projects[better_exposed_filters][version] = "3.0-alpha2"

projects[block_visibility_groups][version] = "1.1"

projects[composer_manager][version] = "1.0-rc2"

projects[core_views_facets][version] = "1.0-alpha4"

projects[ctools][version] = "3.0"

projects[devel][version] = "1.0-rc2"

projects[draggableviews][version] = "1.0"

projects[editor_file][version] = "1.2"

projects[editor_advanced_link][version] = "1.3"

projects[facets][version] = "1.0-alpha7"

projects[fontyourface][version] = "3.1"

projects[pathauto][version] = "1.0-rc1"

projects[search_api][version] = "1.0-beta4"

projects[taxonomy_menu][version] = "3.3"

projects[token][version] = "1.0-rc1"

projects[yamlform][version] = "1.0-beta29"





; Themes

projects[zurb_foundation][version] = "6.0-alpha1"


; Libaries





